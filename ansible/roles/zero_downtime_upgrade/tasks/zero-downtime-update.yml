- name: Stop services on deploy node
  command: gitlab-ctl stop
  when: ('sidekiq_primary' in group_names or ('sidekiq' not in groups and 'gitlab_rails_primary' in group_names))

- name: Create skip-auto-reconfigure file
  file:
    path: /etc/gitlab/skip-auto-reconfigure
    state: touch
    mode: u=rw,g=r,o=r

- name: Disable auto migrations
  template:
    src: templates/zero-downtime.rb.j2
    dest: "/etc/gitlab/gitlab.{{ gitlab_node_type }}.zero-downtime.rb"
  when: ('sidekiq_primary' not in group_names or ('sidekiq' not in groups and 'gitlab_rails_primary' not in group_names))

- name: Update GitLab package
  import_role:
    name: common

- name: Update Gitaly
  include_tasks: update-gitlab.yml
  with_items: "{{ groups['gitaly'] }}"
  vars:
    skip_reconfigure: false
  when: "hostvars[host_item].inventory_hostname == inventory_hostname"
  loop_control:
    loop_var: host_item

- name: Update Praefect
  include_tasks: update-gitlab.yml
  with_items: "{{ groups['praefect'] }}"
  vars:
    skip_reconfigure: false
  when: "hostvars[host_item].inventory_hostname == inventory_hostname"
  loop_control:
    loop_var: host_item

- name: Update Deploy node
  import_tasks: update-gitlab.yml
  vars:
    skip_reconfigure: true
    skip_healthcheck: true
  when: ('sidekiq_primary' in group_names or ('sidekiq' not in groups and 'gitlab_rails_primary' in group_names))

- name: Enable auto migrations and update postgres details on deploy node
  template:
    src: templates/zero-downtime.rb.j2
    dest: "/etc/gitlab/gitlab.{{ gitlab_node_type }}.zero-downtime.rb"
  when: ('sidekiq_primary' in group_names or ('sidekiq' not in groups and 'gitlab_rails_primary' in group_names))

- name: Reconfigure deploy node
  command: gitlab-ctl reconfigure
  environment:
    SKIP_POST_DEPLOYMENT_MIGRATIONS: "true"
  when: ('sidekiq_primary' in group_names or ('sidekiq' not in groups and 'gitlab_rails_primary' in group_names))

- name: Run pre deployment migrations on secondary site when using Geo
  command: gitlab-rake {{ geo_migration_task }}
  environment:
    SKIP_POST_DEPLOYMENT_MIGRATIONS: "true"
  when:
    - ('sidekiq_primary' in group_names or ('sidekiq' not in groups and 'gitlab_rails_primary' in group_names))
    - (geo_secondary_site_group_name in group_names)

- name: Start services on deploy node
  command: gitlab-ctl start
  when: ('sidekiq_primary' in group_names or ('sidekiq' not in groups and 'gitlab_rails_primary' in group_names))

- name: Update all nodes excluding Praefect, Gitaly, GitLab Rails and Deploy nodes.
  include_tasks: update-gitlab.yml
  with_items: "{{ groups.all }}"
  when:
    - "hostvars[host_item].inventory_hostname == inventory_hostname"
    - omnibus_node
    - ('praefect' not in group_names)
    - ('gitaly' not in group_names)
    - ('sidekiq_primary' not in group_names)
    - ('gitlab_rails' not in group_names)
  loop_control:
    loop_var: host_item

- name: Update GitLab Rails
  include_tasks: update-gitlab.yml
  with_items: "{{ groups['gitlab_rails'] }}"
  vars:
    skip_reconfigure: false
    skip_healthcheck: false
  when: "hostvars[host_item].inventory_hostname == inventory_hostname"
  loop_control:
    loop_var: host_item

- name: Restart Sidekiq
  command: gitlab-ctl restart sidekiq
  throttle: 1
  when:
    - ('sidekiq' in group_names)

- name: Restart geo-logcursor on secondary site if present
  command: gitlab-ctl restart geo-logcursor
  throttle: 1
  when:
    - ('gitlab_rails' in group_names)
    - ('gitlab_rails_secondary' in group_names)
